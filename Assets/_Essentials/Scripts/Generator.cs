﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class Generator : MonoBehaviour {
	public GameObject prefab;
	public List<Transform> transforms;
	
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.C))
		{
			CollectChild();
		}

		if (Input.GetKeyDown(KeyCode.G))
		{
			GeneratePrefabs();
		}
	}

	public void CollectChild()
	{
		transforms = new List<Transform>();

		for (int i = 0; i < transform.childCount; i++)
		{
			if (transform.GetChild(i).name.Contains(prefab.name))
				transforms.Add(transform.GetChild(i));
		}
	}

 	public void GeneratePrefabs()
	{
		foreach (Transform t in transforms)
		{
			GameObject p = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
			p.transform.SetParent(t.parent);
			p.transform.position = t.position;
			p.transform.rotation = t.rotation;
		}
	}

	public void DeleteOldChild()
	{
		foreach (Transform t in transforms)
		{
			DestroyImmediate(t.gameObject);
		}

		transforms.Clear();
	}
}
#endif
