﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CutsceneManager : MonoBehaviour
{
    public UnityEvent OnCutsceneFinish;

    public void OnFinish()
    {
        if (OnCutsceneFinish != null)
            OnCutsceneFinish.Invoke();
    }
}
