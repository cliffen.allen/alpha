﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using TMPro;

public enum eLogType{
	TYPE_DEFAULT,
	TYPE_ERROR,
	TYPE_WARNING
}

public class MiscManager : MonoBehaviour {
 private static MiscManager _Instance;

	#region FPS Calculator Variables
	int frameCount = 0;
	float dt = 0.0f;
	float fps = 0.0f;
	float updateRate = 4.0f;  // 4 updates per sec.
	public TextMeshProUGUI fpsLabel;
	public bool isFPS;
	#endregion

	public TextMeshProUGUI universalDebug;

	 #region Debug Variables
	 public bool isDebug;
	 #endregion

	#region MonoBehaviour Methods
 	void Awake()
	{
		Application.targetFrameRate = 60;
		_Instance = this;
	}

	void Update () {
		if (Input.GetKeyDown(KeyCode.F))
		{
			isFPS = !isFPS;
			fpsLabel.gameObject.SetActive(!fpsLabel.gameObject.activeSelf);
		}
		
		if (isFPS)
		{
			frameCount++;
			dt += Time.deltaTime;
			if (dt > 1.0f/updateRate)
			{
				fps = frameCount / dt;
				frameCount = 0;
				dt -= 1.0f/updateRate;
				
				fpsLabel.text = "FPS: " + fps.ToString("F2");
			}
		}
	} 
	#endregion

	#region Member Methods
	public void ResetDebug()
	{
		universalDebug.text = "";
	}
	
	public void SetDebug(bool b)
	{
		isDebug = b;
	}

	public static void DebugLog(object obj, eLogType type = eLogType.TYPE_DEFAULT)
	{
		if (_Instance.isDebug)
		{
			switch (type)
			{
			case eLogType.TYPE_WARNING:
				//Debug.LogWarning(obj);
				break;
			case eLogType.TYPE_ERROR:
				//Debug.LogError(obj);
				break;
			default:
				//Debug.Log(obj);
				_Instance.universalDebug.text += "\n" + obj;
				break;
			}
		}
	}

 	public static void DrawRay(Vector3 start, Vector3 direction, Color col)
	{
		if (_Instance != null && _Instance.isDebug)
			Debug.DrawRay(start, direction, col);
	}

 	public void Reset()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene("TruckTrailer");
	}
	#endregion

	public void ToggleGO(GameObject go)
	{
		go.SetActive(!go.activeSelf);
	}
}
