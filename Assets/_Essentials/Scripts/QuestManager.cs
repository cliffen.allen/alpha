﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
public class QuestManager : MonoBehaviour
{
    public static QuestManager _Instance;
    public int kelapaCount, jamurCount, keyCount;
    public int kelapaTemp, jamurTemp, keyTemp;

    public TextMeshProUGUI questLabel;

    public int timer = 180;
    private float tempTimer;
    private bool day1QuestFinish, day2QuestFinish;
    public UnityEvent OnDayOneFinish, OnDayTwoFinish, OnDayThreeFinish;
    public TextMeshProUGUI questTinyLabel, kelapaLabel, jamurLabel, keyLabel, timerLabel;
    void Awake()
    {
        _Instance = this;   
    }

    public static bool GetProgress(string s)
    {
        bool b = false;
        switch(s)
        {
            case "kelapa":
                b = _Instance.kelapaTemp == _Instance.kelapaCount;
            break;
            case "jamur":
                b = _Instance.jamurTemp == _Instance.jamurCount;
            break;
            case "key":
                b = _Instance.keyTemp == _Instance.keyCount;
            break;
        }

        return b;
    }
    public static void QuestProgress(string s)
    {
        switch(s)
        {
            case "kelapa":
                _Instance.kelapaTemp++;
                _Instance.kelapaLabel.text = _Instance.kelapaTemp + "/" + _Instance.kelapaCount;
            break;
            case "jamur":
                _Instance.jamurTemp++;
                _Instance.jamurLabel.text = _Instance.jamurTemp + "/" + _Instance.jamurCount;
            break;
            case "key":
                _Instance.keyTemp++;
                _Instance.keyLabel.text = _Instance.keyTemp + "/" + _Instance.keyCount;
            break;
        }

        if (!_Instance.day1QuestFinish && _Instance.kelapaTemp == _Instance.kelapaCount && _Instance.jamurTemp == _Instance.jamurCount)
        {
            Debug.Log("Day 1 Quest Complete");
            _Instance.StartCoroutine("Day1QuestFinish");

        }
        else if (!_Instance.day2QuestFinish && _Instance.keyTemp == _Instance.keyCount)
        {
            Debug.Log("Day 2 Quest Complete");
            
            _Instance.StartCoroutine("Day2QuestFinish");
        }
    }

    public void StartLastDay()
    {
        InvokeRepeating("SurvivalCountDown", 0, 1);
    }

    private void SurvivalCountDown()
    {
        timer -= 1;
        System.TimeSpan t = new System.TimeSpan(0, 0, timer);
        timerLabel.text = t.ToString(@"mm\:ss");
        
        if (timer == 0)
        {
            CancelInvoke("SurvivalCountDown");
            StartCoroutine("Day3QuestFinish");
        }
    }

    IEnumerator Day1QuestFinish()
    {
        questLabel.text = "Quest Completed";
        questLabel.gameObject.SetActive(true);
        yield return new WaitForSeconds(2);

        if (OnDayOneFinish != null)
        {
            OnDayOneFinish.Invoke();
        }

        day1QuestFinish = true;
    }

    IEnumerator Day2QuestFinish()
    {
        _Instance.questLabel.text = "Quest Completed!";
        _Instance.questLabel.gameObject.SetActive(true);

        yield return new WaitForSeconds(2);

        if (_Instance.OnDayTwoFinish != null)
        {
            _Instance.OnDayTwoFinish.Invoke();
        }

        day2QuestFinish = true;
    }
    IEnumerator Day3QuestFinish()
    {
        yield return new WaitForSeconds(1f);
        _Instance.questLabel.text = "Quest Completed!";
        _Instance.questLabel.gameObject.SetActive(true);

        yield return new WaitForSeconds(2);

        if (_Instance.OnDayThreeFinish != null)
        {
            _Instance.OnDayThreeFinish.Invoke();
        }
    }
}
