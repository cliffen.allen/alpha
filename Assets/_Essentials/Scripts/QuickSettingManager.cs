﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class QuickSettingManager : MonoBehaviour
{
    public GameObject[] startObjs, firstDialogueObjs, dayOneObjs, dayTwoObj, dayThreeObj;
    
    private void SetObject(int id)
    {
        bool[] b = new bool[]{
            false, false, false, false, false
        };

        b[id] = true;

        foreach(GameObject go in startObjs)
        {
            go.SetActive(b[0]);
        }

        foreach(GameObject go in firstDialogueObjs)
        {
            go.SetActive(b[1]);
        }

        foreach(GameObject go in dayOneObjs)
        {
            go.SetActive(b[2]);
        }

        foreach(GameObject go in dayTwoObj)
        {
            go.SetActive(b[3]);
        }

        foreach(GameObject go in dayThreeObj)
        {
            go.SetActive(b[4]);
        }
    }
    public void SetStart()
    {
        Debug.Log("Activate start objs");
        SetObject(0);
    }

    public void SetFirstDialogue()
    {
        Debug.Log("Activate first dialogue objs");
        SetObject(1);
    }

    public void SetDayOne()
    {
        Debug.Log("Activate day one objs");
        SetObject(2);
        DialogueManager.SetStory = 9;
    }
    public void SetDayTwo()
    {
        Debug.Log("Activate day two objs");
        SetObject(3);
        DialogueManager.SetStory = 10;
    }
    public void SetDayThree()
    {
        Debug.Log("Activate day three objs");
        SetObject(4);
        DialogueManager.SetStory = 15;
    }
}
