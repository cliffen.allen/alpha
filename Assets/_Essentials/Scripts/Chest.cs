﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Chest : MonoBehaviour
{
    public GameObject notif;
    public bool playerInArea;
    public UnityEvent OnOpen;

    private void Update() {
        if (!DialogueManager.InDialogue && playerInArea && Input.GetButtonDown("Fire1"))
        {
            if(QuestManager.GetProgress("key"))
            {
                Debug.Log("Chest Opened");
                if (OnOpen != null)
                    OnOpen.Invoke();
            }
            else
            {
                DialogueManager.ShowDialogue("FindKey");
            }
        }    
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            notif.SetActive(true);
            playerInArea = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            notif.SetActive(false);
            playerInArea = false;
        }
    }
}
