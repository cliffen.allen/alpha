﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleInput : MonoBehaviour
{
    private static SimpleInput _Instance;
    Vector2 movementInput;

    public bool inputBlocked;

    public Vector2 MovementInput{
        get{
            if (inputBlocked)
                return Vector2.zero;
            
            return movementInput;
        }
    }

    void Awake()
    {
        _Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        movementInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }
}
