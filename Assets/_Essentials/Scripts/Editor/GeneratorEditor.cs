﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Generator))]
public class GeneratorEditor : Editor {
 private Generator gen;

	public override void OnInspectorGUI()
 {
  base.OnInspectorGUI();
  gen = (Generator) target;

  if (GUILayout.Button("Generate Object"))
  {
   gen.CollectChild();
   gen.GeneratePrefabs();
   gen.DeleteOldChild();
  }
 }
}
#endif