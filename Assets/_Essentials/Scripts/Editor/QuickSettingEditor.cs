﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(QuickSettingManager))]
public class QuickSettingEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        QuickSettingManager t = target as QuickSettingManager;

        if (GUILayout.Button("Beginning"))
        {
            t.SetStart();
        }
        if (GUILayout.Button("First Dialogue"))
        {
            t.SetFirstDialogue();
        }
        if (GUILayout.Button("Day One"))
        {
            t.SetDayOne();
        }
        if (GUILayout.Button("Day Two"))
        {
            t.SetDayTwo();
        }
        if (GUILayout.Button("Day Three"))
        {
            t.SetDayThree();
        }
    }
}
