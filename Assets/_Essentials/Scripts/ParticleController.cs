﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleController : MonoBehaviour {
 protected ParticleSystem particle;

 void Awake()
 {
  particle = GetComponent<ParticleSystem>();
 }

 public virtual void Disable()
 {
  this.Recycle();
 }
}
