﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class UIManager : MonoBehaviour
{
    public GameObject pauseGO;
    public UnityEvent pauseEvent, unpauseEvent;

    public delegate void UIEventHandler(bool b);
    public static event UIEventHandler OnPause;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !pauseGO.activeSelf)
        {
            SetPause();
        }
    }

    public void SetPause()
    {
        pauseGO.SetActive(!pauseGO.activeSelf);
            
        if (pauseGO.activeSelf)
        {
            pauseEvent.Invoke();
        }
        else
        {
            unpauseEvent.Invoke();
        }

        if (OnPause != null)
        {
            OnPause(pauseGO.activeSelf);
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
