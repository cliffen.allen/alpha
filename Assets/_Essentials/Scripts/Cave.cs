﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Cave : MonoBehaviour
{
    public UnityEvent OnPlayerEnter;

    void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.name);
        if (other.CompareTag("Player"))
        {
            if (OnPlayerEnter != null)
            {
                OnPlayerEnter.Invoke();
            }
        }
    }
}
