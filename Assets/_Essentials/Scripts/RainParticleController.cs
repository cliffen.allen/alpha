﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RainParticleController : ParticleController {
 public float emissionRateLimit = 500, emissionRate, targetRate;
 private ParticleSystem.EmissionModule emission;
 [SerializeField]
 private bool isCalculate;
 private float speed;

 void Update()
 {
  if (isCalculate)
  { 
   emissionRate = Mathf.Lerp(emissionRate, targetRate, Time.deltaTime * speed);
   emission.rateOverTime = emissionRate;

   if (Mathf.Abs(emissionRate-targetRate) < 10f)
   {
    isCalculate = false;

    if (targetRate == 0)
     this.Recycle();
   }
  }
 }
 void OnEnable()
 {
   emission = particle.emission;

  emission.rateOverTime = 0;
  emissionRate = 0;
  targetRate = emissionRateLimit;
  speed = 0.2f;
  isCalculate = true;
 }

 public override void Disable()
 {
  targetRate = 0;
  speed = 0.6f;
  isCalculate = true;
 }
}
