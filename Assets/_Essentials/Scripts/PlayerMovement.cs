﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
	public static PlayerMovement Instance;
	#region Member Variables
	// walkSpeed: player walking speed
	// runSpeed: player running speed
	// angularSmoothing: player rotation speed
	public float walkSpeed, runSpeed, angularSmoothing, gravity = 20;

	private Animator anim;	// player animator

	public bool canMove;
	// isRun: flag for running
	private bool isRun;

	public CharacterController controller;
	public Gamekit3D.CameraSettings cameraSettings;
	#endregion

	#region IMovement Properties
	// see parent
	public float Speed{
		get{ 
			if (!isRun)
				return walkSpeed;
			else
				return runSpeed;
		}
	}

	// see parent
	public float AngularSmoothing
	{
		get{ return angularSmoothing;}
	}
	#endregion

	#region MonoBehaviour Methods
	void Awake()
	{
		anim = GetComponent<Animator> ();
		Instance = this;
	}
	
	void OnEnable() {
		DialogueManager.OnDialogueShow += SetCanMove;
	}

	void OnDisable()
	{
		DialogueManager.OnDialogueShow -= SetCanMove;
	}

	void FixedUpdate() {
		Moving();
	}
	#endregion

	#region Member Methods
	private void SetCanMove(bool b)
	{
		canMove = !b;
	}

	/// <summary>
	/// Moving the player according to the specified horizontal and vertical.
	/// </summary>
	/// <param name="horizontal">Horizontal axis</param>
	/// <param name="vertical">Vertical axis</param>
	public void Moving()
	{
		if (!canMove)
			return;

		// moving the character according to the axis
		float horizontal = Input.GetAxis ("Horizontal");
		float vertical = Input.GetAxis ("Vertical");
		isRun = Input.GetButton ("Jump");

		if (horizontal != 0 || vertical != 0)
		{
			anim.SetFloat("movementSpeed", isRun ? 1 : 0.5f);
			
			Rotating(horizontal, vertical);
			//transform.localPosition = Vector3.MoveTowards (transform.localPosition, transform.TransformPoint(Vector3.forward), Time.deltaTime * Speed);
			//transform.position = Vector3.MoveTowards (transform.position, transform.TransformPoint(Vector3.forward), Time.deltaTime * Speed);
			Vector3 targetPos = transform.TransformDirection(Vector3.forward) * Speed;
			targetPos.y -= gravity * gravity * Time.deltaTime;
			controller.Move(targetPos * Time.deltaTime);
		}
		else
		{
			anim.SetFloat("movementSpeed", 0);
		}
	}
	#endregion

	#region IMovement Methods
	// see parent
	public void Rotating(float horizontal, float vertical)
	{
		Vector3 cameraForward = Quaternion.Euler(0f, cameraSettings.Current.m_XAxis.Value, 0f) * Vector3.forward;
		cameraForward.y = 0f;
		cameraForward.Normalize();
		
		Vector3 cameraRight = new Vector3 (cameraForward.z, 0f, -cameraForward.x);

		Vector3 directionPoint = horizontal * cameraRight + vertical * cameraForward;

		Quaternion targetRotation = Quaternion.LookRotation (directionPoint, Vector3.up);
		Quaternion newRotation = Quaternion.Lerp (transform.localRotation, targetRotation, Time.deltaTime * AngularSmoothing);
		
		transform.localRotation = newRotation;
	}
	#endregion
}