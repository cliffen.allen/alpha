﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlphaController : MonoBehaviour
{
    CharacterController controller;
    Animator anim;
    SimpleInput playerInput;

    private void Awake() {
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();    
        playerInput = GetComponent<SimpleInput>();
    }

    void Move()
    {
        bool b = playerInput.MovementInput == Vector2.zero;

        anim.SetBool("Run", b);
    }
}
