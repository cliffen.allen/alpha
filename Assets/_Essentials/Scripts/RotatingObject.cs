﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingObject : MonoBehaviour
{
    public Vector3 rotateAround;
    public float angle;
    void Update()
    {
        transform.RotateAround(transform.position, rotateAround, angle);
    }
}
