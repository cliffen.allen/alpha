﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectiblesManager : MonoBehaviour
{
    public string collectibleTag = "Collectible";
    public List<GameObject> collectibleList = new List<GameObject>();
    public GameObject notifGO;

    void Update()
    {
        if (Input.GetButtonDown("Fire1") && collectibleList.Count > 0)
        {
            GameObject g = collectibleList[0];
            bool progress = false;
            bool checkProgress = true;
            string objName = g.name.ToLower();
            string id = "";

            if (objName.Contains("kelapa"))
            {
                id = "kelapa";
            }
            else if (objName.Contains("key"))
            {
                id = "key";
            }
            else if (objName.Contains("jamur"))
            {
                id = "jamur";

                if (objName.Contains("merah") || objName.Contains("ungu"))
                {
                    checkProgress = false;
                    DialogueManager.ShowDialogue("Poison");
                }
            }
            
            if (checkProgress)
            {
                progress = QuestManager.GetProgress(id);
                if (!progress)
                {
                    Collect(id);
                    g.SetActive(false);
                    collectibleList.RemoveAt(0);

                    SetNotif();
                }
                else{
                    DialogueManager.ShowDialogue("Enough");
                }
            }
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(collectibleTag))
        {
            collectibleList.Add(other.gameObject);

            SetNotif();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(collectibleTag))
        {
            collectibleList.Remove(other.gameObject);

            SetNotif();
        }
    }

    private void SetNotif()
    {
        if (collectibleList.Count > 0)
            notifGO.SetActive(true);
        else if (collectibleList.Count <= 0)
            notifGO.SetActive(false);
    }

    private void Collect(string s)
    {
        Debug.Log("Progress " + s);
        QuestManager.QuestProgress(s);

        switch(s)
        {
            case "key":
                AudioController .Play("Key");
                break;
        }
    }
}
