﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightCycle : MonoBehaviour
{
    public Material[] sky;
    public Color defaultLightColor;
    public Color[] lightColor;
    private int currentSkybox;
    private float skyBlend;
    public int blendDuration = 60, skyDuration = 10;
    public float step = 0.5f;
    public Light directionalLight;
    // Update is called once per frame
   
    void Start()
    {
        ResetSky();
    }

    public void ResetSky()
    {
        skyBlend = 0;
        currentSkybox = 0;
        RenderSettings.skybox = sky[currentSkybox];
        sky[0].SetFloat("_Blend", 0); 
        directionalLight.color = defaultLightColor;
        
    }

    public void StartBlend()
    {
        skyBlend = 0;
        InvokeRepeating("UpdateBlend", 0, step);
    }

    void UpdateBlend()
    {
        Debug.Log("update blend");
        skyBlend += step;
        sky[currentSkybox].SetFloat("_Blend", skyBlend/blendDuration);
        directionalLight.color = Color.Lerp(directionalLight.color, lightColor[currentSkybox], step/60);

        if (skyBlend == blendDuration)
        {
            CancelInvoke("UpdateBlend");

            if (currentSkybox != 1)
                ChangeSkybox();
        }
    }

    void ChangeSkybox()
    {
        Debug.Log("change skybox");
        currentSkybox = (currentSkybox + 1) % sky.Length;
        sky[currentSkybox].SetFloat("_Blend", 0);
        RenderSettings.skybox = sky[currentSkybox];
        skyBlend = 0;

        InvokeRepeating("UpdateBlend", skyDuration, step);
    }
}
