﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutSceneAudio : MonoBehaviour
{
    public void PlayApi()
    {
        AudioController.Play("Api");
    }
    
    public void PlayAngin()
    {
        AudioController.Play("Angin"); 
    }

    public void PlayThunder()
    {
        AudioController.Play("Thunder"); 
    }

    public void PlayRain()
    {
        AudioController.Play("Rain"); 
    }
  
    public void PlayKelapa()
    {
        AudioController.Play("Kelapa");
    }
}
