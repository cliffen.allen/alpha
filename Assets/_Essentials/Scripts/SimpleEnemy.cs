﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleEnemy : MonoBehaviour
{
    int wpID;
    public Transform[] waypoints;
    public SimpleScanner playerScanner;
    public float speed = 1, angularSpeed = 1;
    public float stopPursuitTimer, distanceThreshold;
    private float tempTimer;
    public bool inPursuit, canMove;

    public CharacterController controller;
    private Transform targetPos;
    private Quaternion targetRot;

    private void OnEnable() {
        UIManager.OnPause += SetPause;
        DialogueManager.OnDialogueShow += SetPause;
    }
    private void OnDisable() {
        UIManager.OnPause -= SetPause;
        DialogueManager.OnDialogueShow -= SetPause;
    }

    void Update()
    {
        if (!canMove)
            return;

        if (!inPursuit)
        {
            targetPos = waypoints[wpID];
            targetRot = Quaternion.LookRotation(targetPos.position - transform.position, Vector3.up);
            //transform.position = Vector3.MoveTowards(transform.position, targetPos.position, Time.deltaTime * speed);
            controller.Move(transform.TransformDirection(Vector3.forward) * Time.deltaTime * speed);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, Time.deltaTime * angularSpeed);
            PlayerMovement player = playerScanner.Detect(transform);

            if (player != null)
            {
                inPursuit = true;
                targetPos = player.transform;
            }

            if (Vector3.Distance(transform.position, targetPos.position) < 0.1f)
            {
                wpID = (wpID + 1) % waypoints.Length;
            }
        }
        else
        {
            targetRot = Quaternion.LookRotation(targetPos.position - transform.position, Vector3.up);
            //transform.position = Vector3.MoveTowards(transform.position, targetPos.position, Time.deltaTime * speed);
            controller.Move(transform.TransformDirection(Vector3.forward) * Time.deltaTime * speed);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, Time.deltaTime * angularSpeed);
            
            if (tempTimer < stopPursuitTimer)
            {
                tempTimer += Time.deltaTime;
            }
            else
            {
                Debug.Log(Vector3.Distance(transform.position, targetPos.position));

                if (Vector3.Distance(transform.position, targetPos.position) > distanceThreshold)
                    inPursuit = false;
    
                tempTimer = 0;
            }
        }
    }

    void SetPause(bool b)
    {
        canMove = !b;
    }

    #if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            playerScanner.EditorGizmo(transform);
        }
#endif
}
