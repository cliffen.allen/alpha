﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {
    public static AudioManager _Instance;

    void Awake()
    {
        if (_Instance != null && _Instance != this)
        {
            DestroyImmediate(this.gameObject);
            return;
        }

        _Instance = this;
        transform.SetParent(null);
        DontDestroyOnLoad(this.gameObject);
    }


    void Start()
    {
        PlayMusic("Lobby");
    }
    public void Play(string id)
    {
        AudioController.Play(id);
    }

    public void PlayMusic(string id)
    {
        AudioController.PlayMusic(id);
    }

    public void StopMusic()
    {
        AudioController.StopMusic(2);
    }
}
