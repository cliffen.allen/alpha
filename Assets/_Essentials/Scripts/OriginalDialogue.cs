﻿using System;
using System.Collections.Generic;
using UnityEngine;

 [CreateAssetMenu]
public class OriginalDialogue : ScriptableObject
{
    public string language;
    public List<Dialogue> phrases = new List<Dialogue>();

    public Dialog this[string key]
    {
        get
        {
            for (int i = 0; i < phrases.Count; i++)
            {
                if (phrases[i].key == key)
                {
                    Dialog dialogue = new Dialog();
                    dialogue.actor = phrases[i].actor;
                    dialogue.value = phrases[i].value;
                    dialogue.target = phrases[i].target;
                    dialogue.ekspresion = phrases[i].ekspresion;

                    return dialogue;
                }
            }

            return null;
        }
    }
}

[Serializable]
public class Dialogue
{
    public string key;
    public string actor;
    public string value;
    public int target;
    public int ekspresion;
}

public class Dialog{
    public string actor;
    public string value;
    public int target;
    public int ekspresion;
}
