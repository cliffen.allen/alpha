﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using TMPro;
public class DialogueManager : MonoBehaviour
{
    private static DialogueManager _Instance;
    public OriginalDialogue dialogue;
    private int storyProgress = 1, dialogueTarget;
    public TextMeshProUGUI actorName, dialogueLabel;
    public float dialogueDelay = 1;
    public GameObject dialogueContainer;
    private Dialog currentDialogue;

    public Image actorImage;
    public Sprite[] alphaFace;
    public Sprite monsterFace;
    public Color otherColor;

    public bool finishDialoue;

    public delegate void OnDialogue(bool b);
    public static event OnDialogue OnDialogueShow;
    public UnityEvent OnFinishStory1, OnFinishStory2, OnFinishStory3, OnFinishStory4, OnFinishStory5, OnFinishStory6,
            OnFinishStory7, OnFinishStory8, OnFinishStory9, OnFinishStory10;

    public static bool InDialogue{
        get{
            return _Instance.dialogueContainer.activeSelf;
        }
    }

    public static int SetStory{
        set{
            _Instance.storyProgress = value;
        }
    }

    void Awake()
    {
        _Instance = this;
    }

    void Update()
    {
        if (!dialogueContainer.activeSelf)
            return;
            
        if (!finishDialoue)
        {
            if (Input.GetMouseButtonDown(0))
                FinishDialogue();
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (dialogueTarget != -1)
                {
                    if (dialogueTarget > 100)
                    {
                        dialogueContainer.SetActive(false);
                        
                        if (OnDialogueShow != null)
                            OnDialogueShow(false);

                        switch(dialogueTarget)
                        {
                            case 101:
                                if (OnFinishStory1 != null)
                                {
                                    OnFinishStory1.Invoke();
                                }
                                storyProgress = 8;
                            break;
                            case 102:
                            if (OnFinishStory2 != null)
                                {
                                    OnFinishStory2.Invoke();
                                }
                                storyProgress = 9;
                            break;
                            case 103:
                            if (OnFinishStory3 != null)
                                {
                                    OnFinishStory3.Invoke();
                                }
                                storyProgress = 10;
                            break;
                            case 104:
                            if (OnFinishStory4 != null)
                                {
                                    OnFinishStory4.Invoke();
                                }
                                storyProgress = 13;
                            break;
                            case 105:
                            if (OnFinishStory5 != null)
                                {
                                    OnFinishStory5.Invoke();
                                }
                                storyProgress = 14;
                            break;
                            case 106:
                            if (OnFinishStory6 != null)
                                {
                                    OnFinishStory6.Invoke();
                                }
                                storyProgress = 15;
                            break;
                            case 107:
                            if (OnFinishStory7 != null)
                                {
                                    OnFinishStory7.Invoke();
                                }
                                storyProgress = 17;
                            break;
                            case 108:
                            if (OnFinishStory8 != null)
                                {
                                    OnFinishStory8.Invoke();
                                }
                                storyProgress = 19;
                            break;
                            case 109:
                            if (OnFinishStory9 != null)
                                {
                                    OnFinishStory9.Invoke();
                                }
                                storyProgress = 24;
                            break;
                            case 110:
                            if (OnFinishStory10 != null)
                                {
                                    OnFinishStory10.Invoke();
                                }
                            break;
                        }
                        
                        dialogueTarget = -1;
                    }
                    else{
                        storyProgress = dialogueTarget + 1;
                        GetStoryDialogue();
                    }
                }
                else
                {
                    dialogueContainer.SetActive(false);

                    if (OnDialogueShow != null)
                        OnDialogueShow(false);
                }
            }
        }
    }

    public void GetStoryDialogue()
    {
        string id = "Story" + storyProgress;
        currentDialogue = dialogue[id];
        finishDialoue = false;
        dialogueTarget = currentDialogue.target;
        _Instance.ShowDialogue();
    }

    private void FinishDialogue()
    {
        StopCoroutine("StartDialogue");
        dialogueLabel.text = currentDialogue.value;
        finishDialoue = true;
    }

    public static void ShowDialogue(string key)
    {
        Debug.Log(InDialogue);
        if (InDialogue)
            return;

        _Instance.currentDialogue = _Instance.dialogue[key];
        _Instance.finishDialoue = false;
        _Instance.dialogueTarget = _Instance.currentDialogue.target;
        Debug.Log(_Instance.dialogueTarget);
        _Instance.ShowDialogue();
    }
    private void ShowDialogue()
    {
        dialogueContainer.SetActive(true);
        if (OnDialogueShow != null)
            OnDialogueShow(true);

        actorName.text = currentDialogue.actor;

        if (actorName.text == "Alpha")
        {
            actorImage.sprite = alphaFace[currentDialogue.ekspresion];
            actorImage.color = Color.white;
        }
        else if (actorName.text == "Monster")
        {
            actorImage.sprite = monsterFace;
            actorImage.color = Color.white;
        }
        else
        {
            actorImage.color = otherColor;
        }

        StartCoroutine("StartDialogue");
    }

    IEnumerator StartDialogue()
    {
        string d = "";
        int id = 0;

        while(id < currentDialogue.value.Length)
        {
            d += currentDialogue.value[id];
            dialogueLabel.text = d;
            id++;

            yield return new WaitForSeconds(dialogueDelay);
        }

        finishDialoue = true;
    }
}
